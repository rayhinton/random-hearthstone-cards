# Random Hearthstone Cards

Some raw random Hearthstone cards created via [instructions](https://www.reddit.com/r/Flamewanker/comments/ctic71/card_generation_tutorial/) from u/The_Giffer on r/Flamewanker.

In the future, I might also generate some output based on u/NTaya's [post about GPT-2](https://www.reddit.com/r/Flamewanker/comments/bpog3j/and_now_for_something_completely_different_i/).

## Credits

Hearthstone-Card-Data.txt courtesy u/The_Giffer.
